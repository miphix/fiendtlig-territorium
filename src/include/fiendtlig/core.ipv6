flush_rules6()
{

  ${raw6} -F; ${raw6} -X
  ${raw6} -P PREROUTING ACCEPT
  ${raw6} -P OUTPUT ACCEPT

  ${man6} -F; ${man6} -X
  ${man6} -P PREROUTING ACCEPT
  ${man6} -P INPUT ACCEPT
  ${man6} -P FORWARD ACCEPT
  ${man6} -P OUTPUT ACCEPT
  ${man6} -P POSTROUTING ACCEPT

  ${nat4} -F; ${nat4} -X
  ${nat4} -P PREROUTING ACCEPT
  ${nat4} -P INPUT ACCEPT
  ${nat4} -P OUTPUT ACCEPT
  ${nat4} -P OUTPUT ACCEPT
  ${nat4} -P POSTROUTING ACCEPT

  ${ipv6} -F; ${ipv6} -X
  ${ipv6} -P INPUT ACCEPT
  ${ipv6} -P FORWARD ACCEPT
  ${ipv6} -P OUTPUT ACCEPT

  ${sec6} -F; ${sec6} -X
  ${sec6} -P INPUT ACCEPT
  ${sec6} -P FORWARD ACCEPT
  ${sec6} -P OUTPUT ACCEPT

}

core_rules6()
{

  ## Seating default position before permitting configuration
  ${ipv6} -P INPUT DROP
  ${ipv6} -P FORWARD DROP
  ${ipv6} -P OUTPUT DROP

  ${ipv6} -A INPUT -i lo -j ACCEPT
  ${ipv6} -A OUTPUT -o lo -j ACCEPT

  ## Regardless of configuration rules
  # Any dissallow will occur before accept, as we're dropping by default

  ## Configuring developer defined chains
  # -- 'mangle-LAD6'
  ${man6} -N mangle-LAD6
  ${man6} -A mangle-LAD6 -m limit --limit 7/min \
   -j LOG --log-prefix 'ipt: mangle DROP: ' --log-level 7
  ${man6} -A mangle-LAD6 -j DROP

  # -- 'mangle-iTCP6'
  ${man6} -N mangle-iTCP6
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags FIN,SYN FIN,SYN -j mangle-LAD6
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags SYN,RST SYN,RST -j mangle-LAD6
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags FIN,RST FIN,RST -j mangle-LAD6
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags FIN,ACK FIN -j mangle-LAD6
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags ACK,URG URG -j mangle-LAD6
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags ACK,PSH PSH -j mangle-LAD6
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags ALL NONE -j mangle-LAD6
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags RST RST -m limit --limit 2/s \
   --limit-burst 2 -j RETURN
  ${man6} -A mangle-iTCP6 -p tcp --tcp-flags RST RST -j mangle-LAD6

  # -- 'mangle-iUDP6'

  # -- 'filter-LAD6
  ${ipv6} -N filter-LAD6
  ${ipv6} -A filter-LAD6 -m limit --limit 7/min \
   -j LOG --log-prefix "ipt: filter DROP: " --log-level 7
  ${ipv6} -A filter-LAD6 -j DROP

  # -- 'filter-LAR6'
  ${ipv6} -N filter-LAR6
  ${ipv6} -A filter-LAR6 -m recent --name logShield --rcheck \
   --hitcount 5 --seconds 5 -j filter-LAD6
  ${ipv6} -A filter-LAR6 -m recent --name logShield --rcheck \
   --hitcount 45 --seconds 60 -j filter-LAD6
  ${ipv6} -A filter-LAR6 -m recent --name logShield --set \
   -j LOG --log-prefix "ipt: filter REJECT: " --log-level 4
  ${ipv6} -A filter-LAR6 -p tcp -j REJECT
  ${ipv6} -A filter-LAR6 -p udp -j REJECT --reject-with icmp6-port-unreachable
  ${ipv6} -A filter-LAR6 -j REJECT

  # -- 'filter-iTCP6-PSCAN'
  ${ipv6} -N filter-iTCP6-PSCAN
  ${ipv6} -A filter-iTCP6-PSCAN -p tcp -m recent \
   --name TCP6-PSCAN --rcheck --hitcount 20 --seconds 2 \
   -j LOG --log-prefix "ipt: WARNING! Scan in progress! " --log-level 7
  ${ipv6} -A filter-iTCP6-PSCAN -p tcp -m recent \
   --name TCP6-PSCSN --rcheck --hitcount 2 --seconds 58 \
   -j LOG --log-prefix "ipt: ATTENTION! Possible scan in progress! " --log-level 4
  ${ipv6} -A filter-iTCP6-PSCAN -p tcp -m recent \
   --name TCP6-PSCAN --rcheck -j filter-LAR6

  # -- 'filter-iUDP6-PSCAN'
  ${ipv6} -N filter-iUDP6-PSCAN
  ${ipv6} -A filter-iUDP6-PSCAN -p udp -m recent \
   --name UDP6-PSCAN --rcheck --hitcount 20 --seconds 20 \
   -j LOG --log-prefix "ipt: WARNING! Scan in progress! " --log-level 7
  ${ipv6} -A filter-iUDP6-PSCAN -p udp -m recent \
   --name UDP6-PSCAN --rcheck --hitcount 2 --seconds 58 \
   -j LOG --log-prefix "ipt: ATTENTION! Possible scan in progress! " --log-level 4
  ${ipv6} -A filter-iUDP6-PSCAN -p udp -m recent \
   --name UDP6-PSCAN --rcheck -j filter-LAR6

  # -- 'filter-PSCAN6'
  ${ipv6} -N filter-PSCAN6
  ${ipv6} -A filter-PSCAN6 \
   -j LOG --log-prefix "ipt: ATTENTION! Possible scan in progress: " --log-level 4
  ${ipv6} -A filter-PSCAN6 -p tcp -m recent --set --rsource \
   --name TCP6-PSCAN -j REJECT
  ${ipv6} -A filter-PSCAN6 -p udp -m recent --set --rsource \
   --name UDP6-PSCAN -j REJECT --reject-with icmp6-port-unreachable
  ${ipv6} -A filter-PSCAN6 -j REJECT

  # -- 'filter-iEST6'

  # -- 'filter-iTCP6'
  ${ipv6} -N filter-iTCP6
  ${ipv6} -A filter-iTCP6 -p tcp -m connlimit --connlimit-above 80 \
   -j REJECT
  ${ipv6} -A filter-iTCP6 -p tcp -m recent --update --rsource --seconds 60 \
   --name TCP6-PSCAN -j filter-iTCP6-PSCAN
  ${ipv6} -A filter-iTCP6 -p tcp -m tcpmss ! --mss 536:65535 -j filter-iTCP6-PSCAN

  # -- 'filter-iUDP6'
  ${ipv6} -N filter-iUDP6
  ${ipv6} -A filter-iUDP6 -p udp -m connlimit --connlimit-above 80 \
   -j REJECT --reject-with icmp6-port-unreachable
  ${ipv6} -A filter-iUDP6 -p udp -m recent --update --rsource --seconds 60 \
   --name UDP6-PSCAN -j filter-iUDP6-PSCAN

  # -- 'filter-oTCP6-www'
  ${ipv6} -N filter-oTCP6-www
  ${ipv6} -A filter-oTCP6-www -p tcp -m multiport --dports 80,443 -j ACCEPT

  # -- 'filter-oTCP6-ssh'
  ${ipv6} -N filter-oTCP6-ssh 
  ${ipv6} -A filter-oTCP6-ssh -p tcp -m multiport --dports 22,2222 -j ACCEPT

  # -- 'filter-oTCP6-dns'
  #{ipv6} -N filter-oTCP6-dns
  #{ipv6} -A filter-oTCP6-dns -p tcp -m multiports --dports 53,5353 -j ACCEPT
  #{ipv6} -A filter-oTCP6-dns -p tcp -m multiports --dports 443,843 -j ACCEPT

  # -- 'filter-oTCP6'
  ${ipv6} -N filter-oTCP6
  ${ipv6} -A filter-oTCP6 -p tcp -m owner --gid-owner ${http_clients} \
   --suppl-groups -j filter-oTCP6-www
  ${ipv6} -A filter-oTCP6 -p tcp -m owner --gid-owner ${ssh_clients} \
   --suppl-groups -j filter-oTCP6-ssh

  # -- 'filter-oUDP6-dns'
  ${ipv6} -N filter-oUDP6-dns
  ${ipv6} -A filter-oUDP6-dns -p udp -m multiport --dports 53,5353 -j ACCEPT

  # -- 'filter-oUDP6'
  ${ipv6} -N filter-oUDP6
  ${ipv6} -A filter-oUDP6 -p udp -m owner --gid-owner ${dns_clients} \
   --suppl-groups -j filter-oUDP6-dns

  # -- 'filter-oEST6'

  ## PREROUTING: raw, nat, mangle
  #+  Applies to packets that enter the network interface card

  # - raw
  ${raw6} -A PREROUTING -s ff02::/16 -j DROP
  ${raw6} -A PREROUTING -s ffe8::/16 -j DROP
  ${raw6} -A PREROUTING -s ::/8 -j DROP
  ${raw6} -A PREROUTING -s ::1 ! -i lo -j DROP
  # - mangle
  ${man6} -A PREROUTING -m conntrack --ctstate INVALID -j mangle-LAD6
  ${man6} -A PREROUTING -p tcp -j mangle-iTCP6
  # - nat

  ## INPUT: filter, mangle
  #+  Applies to packets destined to a local socket

  # - mangle
  # - filter
  ${ipv6} -A INPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT # filter-iEST6
  ${ipv6} -A INPUT -p ipv6-icmp -m conntrack --ctstate NEW -j DROP
  ${ipv6} -A INPUT -p ipv6-icmp -j ACCEPT
  ${ipv6} -A INPUT -p udp -m conntrack --ctstate NEW -j filter-iUDP6
  ${ipv6} -A INPUT -p tcp --syn -m conntrack --ctstate NEW \
   -m limit --limit 60/s --limit-burst 20 -j filter-iTCP6
  ${ipv6} -A INPUT -j filter-PSCAN6

  ## FORWARD: filter, mangle
  #+  Applies to packets that are being routed through the server

  # - mangle
  # - filter

  ## OUTPUT: raw, filter, nat, mangle
  #+  Applies to packets that the server sends (locally generated)

  # - raw
  # - mangle
  # - nat
  # - filter
  ${ipv6} -A OUTPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT # filter-oEST6
  ${ipv6} -A OUTPUT -p ipv6-icmp -j ACCEPT
  ${ipv6} -A OUTPUT -p udp -m conntrack --ctstate NEW -j filter-oUDP6
  ${ipv6} -A OUTPUT -p tcp -m conntrack --ctstate NEW -j filter-oTCP6
  ${ipv6} -A OUTPUT -j REJECT

  ## PREROUTING: nat, mangle
  #+  Applies to packets that leave the server

  # - mangle
  # - nat

}

