# Adds to qualities to bash specific to using this environment. Expect a script
# in bin/ to source this so that you may begin work on the project as if it is
# infact installed.


# Add the appropriate directories to path for availability without isntallation
if [ -x "${PWD}/bin" ]; then
    PATH="${PWD}/bin:$PATH"
fi

if [ -x "${PWD}/src/bin" ]; then
   PATH="${PWD}/src/bin:$PATH"
fi

if [ -x "${PWD}/src/sbin" ]; then
   PATH="${PWD}/src/sbin:$PATH"
fi

# Adding variables for cross polination of variables to populate across the
# underlying variables found in the scripts within the newly defined PATH.
root_dir="${PWD}/src"
data_dir="${PWD}/resources"
func_lib="${PWD}/src/include/fiendtlig"
core_ip4="${PWD}/src/include/fiendtlig/core.ipv4"
core_ip6="${PWD}/src/include/fiendtlig/core.ipv6"
roll_tgt="${PWD}/src/include/fiendtlig"
vars_dir="${PWD}/src/lib/fiendtlig"
var_file="${vars_dir}/territorium.var"
